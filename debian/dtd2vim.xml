<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
  "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd" [

  <!ENTITY dhfirstname "<firstname>Stefano</firstname>">
  <!ENTITY dhsurname   "<surname>Zacchiroli</surname>">
  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "<date>05 Feb 2007</date>">
  <!ENTITY dhsection   "<manvolnum>1</manvolnum>">
  <!ENTITY dhemail     "<email>zack@debian.org</email>">
  <!ENTITY dhusername  "Stefano Zacchiroli">
  <!ENTITY dhucpackage "<refentrytitle>dtd2vim</refentrytitle>">
  <!ENTITY dhpackage   "dtd2vim">

  <!ENTITY debian      "<productname>Debian</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
  <!ENTITY gpl         "&gnu; <acronym>GPL</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2007</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>creates XML data file for Vim7 omni completion from
      DTDs</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
      <arg choice="req"><replaceable>filename</replaceable>.dtd</arg>
      <arg choice="opt"><replaceable>dialectname</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1>
    <title>DESCRIPTION</title>

    <para> This manual page documents brieftly the
      <command>&dhpackage;</command> program. For more information see its HTML
      documentation in
      <filename>/usr/share/doc/vim-scripts/html/dtd2vim.html</filename>.
    </para>

    <para>Starting from version 7 Vim supports context aware completion of XML
      files (and others). In particular, when the file being edited is an XML
      file, completion can be driven by the grammar extracted from a Document
      Type Definition (DTD). </para>

    <para>For this feature to work the user should put an XML data file
      corresponding to the desired DTD in a <filename>autoload/xml</filename>
      directory contained in a directory belonging to Vim's
      <varname>'runtimepath'</varname> (for example
      <filename>~/.vim/autoload/xml/</filename>). </para>

    <para><command>&dhpackage;</command> is the program that creates XML data
      files from DTDs. Given as input a DTD
      <filename><replaceable>file</replaceable>.dtd</filename> it will create a
      <filename>file.vim</filename> XML data file.
      <replaceable>dialectname</replaceable> will be part of dictionary name
      and will be used as argument for the <command>:XMLns</command> command.
    </para>

  </refsect1>

  <refsect1>
    <title>OPTIONS</title>

    <para>None. </para>
  </refsect1>

  <refsect1>
    <title>SEE ALSO</title>

    <para>vim (1).</para>

    <para>In the Vim online help: <userinput>:help compl-omni</userinput>,
      <userinput>:help ft-xml-omni</userinput>, <userinput>:help
	:XMLns</userinput>. </para>

    <para>dtd2vim is fully documented in
      <filename>/usr/share/doc/vim-scripts/html/dtd2vim.html</filename>.
    </para>
  </refsect1>

  <refsect1>
    <title>AUTHOR</title>

    <para>This manual page was written by &dhusername; &dhemail; for the
      &debian; system (but may be used by others). Permission is granted to
      copy, distribute and/or modify this document under the terms of the &gnu;
      General Public License, Version 2 any later version published by the Free
      Software Foundation. </para>

    <para> On Debian systems, the complete text of the GNU General Public
      License can be found in /usr/share/common-licenses/GPL. </para>
  </refsect1>

</refentry>

