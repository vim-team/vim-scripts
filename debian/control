Source: vim-scripts
Section: editors
Priority: optional
Maintainer: Debian Vim Maintainers <team+vim@tracker.debian.org>
Uploaders: James McCoy <jamessan@debian.org>
Build-Depends: cdbs, debhelper (>> 10), dh-vim-addon, quilt
Build-Depends-Indep: xsltproc, docbook-xsl
Rules-Requires-Root: no
Standards-Version: 4.6.0
Homepage: http://www.vim.org/scripts/
Vcs-Git: https://salsa.debian.org/vim-team/vim-scripts.git
Vcs-Browser: https://salsa.debian.org/vim-team/vim-scripts

Package: vim-scripts
Architecture: all
Depends: ${misc:Depends}, ${vim-addon:Depends}
Suggests: perlsgml, libtemplate-perl, ctags
Description: plugins for vim, adding bells and whistles
 Vim is a very capable editor. Its scripting support allows the use of plugins
 that enhance the functionality of it. Many people have written scripts, they
 are scattered all over the web; however, there's a central resource on
 https://www.vim.org/ to start with.
 .
 This is a collection of some of those scripts on a purely subjective and biased
 basis.  Users can thus have some nice scripts without having to go searching.
 The scripts can be installed globally or on a per user basis.  Here is a
 summary of the included scripts:
 .
  * AlignPlugin - Provides commands to help produce aligned text, eqns,
    declarations
  * alternateFile - Alternate Files quickly (.c --> .h etc)
  * bufexplorer - Easily switch between buffers without knowing their numbers
  * calendar - Calendar
  * closetag - Easily close SGML-like tags
  * color_sampler_pack - over 100 different color schemes
  * cvsmenu - CVS menu supporting most CVS commands
  * debPlugin - for browsing Debian packages
  * detectindent - Automatically determine indent settings
  * doxygen-toolkit - Simplify Doxygen documentation
  * dtd2vim - create XML data file for XML omni-completion from DTDs
  * EnhancedCommentify - Quickly comment lines in a program
  * gnupg - Transparent editing of gpg encrypted files
  * info - GNU info documentation browser
  * lbdbq - Easily query lbdb from Vim
  * minibufexpl - Elegant buffer explorer - takes very little screen space
  * nerd-commenter - easy code commenting
  * omnicppcomplete - C/C++ omni-completion with ctags
  * po - Easier editing of PO multi-lingual translation files
  * project - organize and navigate file projects
  * python-indent - alternative Python indent script
  * secure-modelines - secure, user-configurable modeline support
  * snippetsEmu - emulate TextMate's snippet expansion
  * supertab - use the tab key for all insert-mode completion
  * surround - easily delete, change, and add 'surround' of text
  * taglist - Source code browser (supports dozens of languages)
  * tetris - Tetris for Vim
  * utl - Univeral Text Linking - Execute URLs in plain text
  * VCSCommand - Commands for cvs, svn, git, hg, bzr, svk
  * vimplate - template system based on template-toolkit
  * VimSokoban - Sokoban Game for Vim
  * whatdomain - Find out the meaning of any Top Level Domain
  * winmanager - A windows style IDE for Vim 6.0
  * xmledit - Helper for editing XML, HTML, and SGML documents
 .
 All of the addons are installed as "optional" Vim packages.  They can be
 enabled by adding "packadd! <addon>" (e.g., "packadd! AlignPlugin") to the
 vimrc.
